<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Simple PHP Calculator</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans&display=swap" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h1 id="siteTitle">Simple PHP Calculator</h1>
            <form method="POST">
                <input type="text" name="num1" id="num1">
                <input type="text" name="num2" id="num2">
                <select id="operators" name="operators">
                    <option value="None">None</option>
                    <option value="Add">Add</option>
                    <option value="Subtract">Subtract</option>
                    <option value="Multiply">Multiply</option>
                    <option value="Divide">Divide</option>
                    <option></option>
                </select>
                <button type="submit" name="submit" value="submit">Calculate</button>
                <br>
                Result : <?php 
                    if(isset($_POST['submit'])){
                        $result1 = $_POST['num1'];
                        $result2 = $_POST['num2'];
                        $operators = $_POST['operators'];
                        switch($operators){
                            case "None":
                                echo "Error! Please Select Somthing";
                                break 1;
                            case "Add":
                                echo (int)$result1 + (int)$result2;
                                break 1;
                            case "Subtract":
                                echo (int)$result1 - (int)$result2;
                                break 1;
                            case "Multiply":
                                echo (int)$result1 * (int)$result2;
                                break 1;
                            case "Divide":
                                echo (int)$result1 / (int)$result2;
                                break 1;
                        }
                    }
                ?>
            </form>
        </div>
    </body>
</html>
